FROM golang:alpine as builder

RUN apk add \
    --no-cache \
    git ca-certificates gcc musl-dev

RUN mkdir /app
WORKDIR /app
COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /autotag ./autotag/

FROM alpine:latest

RUN apk add --no-cache git curl

COPY --from=builder /autotag /autotag

ENTRYPOINT [ "/autotag" ]